use std::{io::Read, str::FromStr};

#[derive(Debug)] // Debug necessary to print it
enum InputError {
    ReadInputError,
    IntParseError,
}

fn get_input() -> Result<Vec<i32>, InputError> {
    //! Read stdin line by line, converts each line's text into an i32 integer,
    //! and returns the integer values as a vector.
    //!
    //! It returns when EOF is reached.
    //!
    //! If one line from stdin cannot be parsed into a string, or if reading
    //! from stdin fails, an error is returned.
    let mut string = String::new();
    std::io::stdin()
        .read_to_string(&mut string)
        .map_err(|_| InputError::ReadInputError)?;
    string
        .lines()
        .map(|v| i32::from_str(v).map_err(|_| InputError::IntParseError))
        .collect()
}

#[derive(PartialEq)] // PartialEq necessary to compare enum elements with ==
enum Delta {
    Deeper,
    Higher,
    Equal,
}

impl Delta {
    fn from_measurements(first: &i32, second: &i32) -> Delta {
        if first > second {
            Delta::Higher
        } else if first < second {
            Delta::Deeper
        } else {
            Delta::Equal
        }
    }
}

fn going_deeper_count(values: &Vec<i32>) -> usize {
    //! Algorithm for riddle number one.
    values
        .windows(2)
        .map(|window| Delta::from_measurements(&window[0], &window[1]))
        .filter(|delta| *delta == Delta::Deeper)
        .count()
}

fn going_deeper_smoothed_count(values: &Vec<i32>, window_length: usize) -> usize {
    //! Algorithm for riddle number two.
    going_deeper_count(
        &values
            .windows(window_length)
            .map(|window| window.iter().sum())
            .collect::<Vec<i32>>(),
    )
}

fn main() {
    match get_input() {
        Ok(values) => {
            println!("We will go deeper {} times!", going_deeper_count(&values));
            println!(
                "Smoothing out the noise, we expect to go deeper {} times!",
                going_deeper_smoothed_count(&values, 3)
            )
        }
        Err(e) => {
            println!("Input error: {:?}", e)
        }
    }
}

#[cfg(test)]
mod test_going_deeper_count {
    #[test]
    fn going_only_deeper() {
        let measurements = vec![1, 2, 3];
        let result = super::going_deeper_count(&measurements);
        assert_eq!(result, 2);
    }

    #[test]
    fn going_only_higher() {
        let measurements = vec![3, 2, 1];
        let result = super::going_deeper_count(&measurements);
        assert_eq!(result, 0);
    }

    #[test]
    fn one_measurement_results_in_zero() {
        let measurements = vec![42];
        let result = super::going_deeper_count(&measurements);
        assert_eq!(result, 0);
    }

    #[test]
    fn no_measurements_results_in_zero() {
        let measurements = vec![];
        let result = super::going_deeper_count(&measurements);
        assert_eq!(result, 0);
    }
}

#[cfg(test)]
mod test_going_deeper_smoothed_count {
    #[test]
    fn going_deeper_with_two_windows() {
        let measurements = vec![1, 2, 3, 4];
        // gives smoothed values [6, 9], ergo we go deeper one time
        let result = super::going_deeper_smoothed_count(&measurements, 3);
        assert_eq!(result, 1);
    }

    #[test]
    fn no_measurement_results_in_zero() {
        let measurements = vec![];
        let result = super::going_deeper_smoothed_count(&measurements, 3);
        assert_eq!(result, 0);
    }

    #[test]
    fn going_deeper_twice_with_four_windows() {
        let measurements = vec![1, 2, 3, 4, 5, 1];
        // gives smoothed values [6, 9, 12, 10], ergo we go deeper two times
        let result = super::going_deeper_smoothed_count(&measurements, 3);
        assert_eq!(result, 2);
    }
}
